﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;

namespace TcpIap
{
    public partial class Form1 : Form
    {
        byte[] datatosend = new byte[1024];                      //定义数据发送缓冲区
        byte[] datarec = new byte[1024];                         //定义数据发送缓冲区
        int packet_zheng = 0;                                    //定义固件大小的整K字节数
        int packet_yu = 0;                                       //定义固件大小的不足1K字节个数
        int packet_send = 0;                                     //定义已经发送的整K字节个数
        FileStream fs;                                           //定义文件流
        string str;                                              //定义用于显示信息的字符串   
        NetworkStream mystream;                                  //定义用于网络传输的数据流
        IPEndPoint myendpoint;                                   //定义本机的IP地址
        TcpClient myclient=null;                                 //定义用于数据传输的TcpClient
        Thread thread_recdata;                                   //定义数据接收线程
        public Form1()
        {
            InitializeComponent();                               //初始化各个控件
            Form1.CheckForIllegalCrossThreadCalls = false;       //可跨线程使用控件
            btn_send.Enabled = false;                            //禁用发送/下载按钮
            btn_boot.Enabled = false;                            //禁用进入BootLoader按钮
            btn_open.Enabled = false;                            //禁用打开固件按钮
            btn_connect.Enabled = true;                          //使能连接按钮
            btn_disconnect.Enabled = false;                      //禁用断开连接按钮
          
                                                                                     
        }
        private void button1_Click(object sender, EventArgs e)                       //发送下载程序命令
        {
            datatosend[0]=0x44;
            datatosend[1] = 0x4d;
            datatosend[2] = 0x46;
            mystream.Write(datatosend, 0, 3);
            btn_send.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)                      //选择固件按钮
        {
            OpenFileDialog openfile = new OpenFileDialog();                         //打开文件控件
            try
            {
                openfile.ShowDialog();                                                  //显示打开文件对话框
                txt_filename.Text = openfile.FileName;                                  //获取所选择固件的名称
                fs = new FileStream(openfile.FileName, FileMode.Open);                  //获取文件流
                str = "文件共" + fs.Length.ToString() + "字节" + "\n";                      //获取文件的总字节数
                textBox1.AppendText(str);                                               //显示文件的总字节数
                packet_zheng = (int)fs.Length / 1024;                                   //获取文件的整K字节数
                packet_yu =(int) fs.Length % 1024;                                      //获取不足1K的剩余字节数
                btn_open.Enabled = false;                                               //禁用选择固件文件按钮
                btn_send.Enabled = true; 
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                MessageBox.Show("请重新选择固件");
            }
            
                                                       //使能发送/下载数据按钮
            
        }
        private void send_length()                                                  //发送固件的长度数据
        {
            datatosend[0] = 0xb1;
            datatosend[1] = 0xb2;
            datatosend[2] = 0xb3;
            datatosend[3] = (byte)(packet_zheng / 256);                             //获取整K字节数的高八位
            datatosend[4] = (byte)(packet_zheng % 256);                             //获取整K字节数的低八位
            datatosend[5] = (byte)(packet_yu / 256);                                //获取不足1K字节数的高八位
            datatosend[6] = (byte)(packet_yu % 256);                                //获取不足1K字节数的低八位
            datatosend[7] = 0x1b;
            datatosend[8] = 0x2b;
            datatosend[9] = 0x3b;
            mystream.Write(datatosend, 0, 10);

        }
        private void send_data_zheng()                                              //发送整K字节数据
        {
            fs.Read(datatosend, 0, 1024);
            mystream.Write(datatosend, 0, 1024);
        }

        private void send_data_yu()                                                //发送不足1K字节数据
        {
            fs.Read(datatosend, 0, packet_yu);
            mystream. Write(datatosend, 0, packet_yu);
        }

        private void btn_reset_Click(object sender, EventArgs e)                  //发送进入BootLoader命令
        {   
            if (myclient.Connected == false)
            {
                myclient.Connect(txtip.Text, 1550);
            }
            datatosend[0] = 0xe1;
            datatosend[1] = 0xe2;
            datatosend[2] = 0xe3;
            datatosend[3] = 0x1e;
            datatosend[4] = 0x2e;
            datatosend[5] = 0x0d;
            datatosend[6] = 0x0a;
            mystream .Write(datatosend, 0, 7);

        }

        private void receive_data()                         //数据接收线程
        {

            while (myclient.Connected)                    
            {
                if (mystream.CanRead)                            //当有数据可读时
                {

                    int len = (int)myclient.ReceiveBufferSize;   //获取数据的长度
                    datarec = new byte[len];                     //定义数据缓冲数组
                    
                    mystream.Read(datarec, 0, len);              //读取数据到数组
                    if (len >= 5)
                    {
                        if (datarec[0] == 0xf1 & datarec[1] == 0xf2 & datarec[2] == 0xf3 & datarec[3] == 0x1f & datarec[4] == 0x2f)          //判断是否已经连接到终端
                        {
                            str = "已连接到设备" + "\n";  
                            textBox1.AppendText(str);       //显示状态信息
                            btn_open.Enabled = true;
                            btn_boot.Enabled = true;
                        }
                        else if (datarec[0] == 0xa1 & datarec[1] == 0xa2 & datarec[2] == 0xa3 & datarec[3] == 0x2a & datarec[4] == 0x1a)     //判断终端是否收到连接信息
                        {

                            str = "客户端已确认下载信息" + "\n";
                            textBox1.AppendText(str);      //显示状态信息
                            send_length();                 //发送长度数据
                        }
                        else if (datarec[0] == 0xb1 & datarec[1] == 0xb2 & datarec[2] == 0xb3 & datarec[3] == 0x2b & datarec[4] == 0x1b)     //判断终端是否接受到长度消息
                        {
                            str = "客户端接收长度信息完毕,即将开始下载数据" + "\n";
                            textBox1.AppendText(str);      //显示状态信息
                            send_data_zheng();             //发送整K字节数据
                        }
                        else if (datarec[0] == 0xc1 & datarec[1] == 0xc2 & datarec[2] == 0xc3 & datarec[3] == 0x2c & datarec[4] == 0x1c)     //判断终端1K字节数据是否接受完毕
                        {

                            packet_send++;
                            str = "已下载" + packet_send.ToString() + "K" + "\n";     //显示已经发送的字节数
                            textBox1.AppendText(str);
                            if (packet_send < packet_zheng)                           //若整K字节数未发送完
                            {
                                send_data_zheng();                                    //发送整K字节数据
                            }
                            else if (packet_send == packet_zheng)                     //若整K字节数据已发送完
                            {
                                send_data_yu();                                       //发送余下不足1K的数据
                            }
                        }
                        else if (datarec[0] == 0xd1 & datarec[1] == 0xd2 & datarec[2] == 0xd3 & datarec[3] == 0x2d & datarec[4] == 0x1d)     //判断终端是否完全接受完数据
                        {
                            str = "程序下载完毕" + "\n";
                            textBox1.AppendText(str);
                            fs.Flush();                             //释放流资源
                            fs.Dispose();
                            packet_send = 0;                        //发送整K字节计数清零
                          
            
                            myclient.Close();                       //释放TcpClient资源
                            myclient = null;
                            mystream.Flush();                       //释放网络流资源

                            btn_connect.Enabled = true;             //使能连接按钮
                            btn_disconnect.Enabled = false;         //禁用断开连接按钮
                            btn_open.Enabled = false;               //禁用选择数据按钮
                            btn_boot.Enabled = false;               //禁用进入BootLoader按钮
                            btn_send.Enabled=false;                 //禁用发送/下载数据按钮

                            thread_recdata.Abort();                 //终端接收数据线程 
                          
                        }
                        else if (datarec[0] == 0xe1 & datarec[1] == 0xe2 & datarec[2] == 0xe3 & datarec[3] == 0x1e & datarec[4] == 0x2e)    //判断终端是否成功进入BootLoader
                        {
                            str = "进入bootloader成功" + "\n";
                            textBox1.AppendText(str);
                           
                            myclient.Close();                      //释放TcpClient资源
                            myclient = null;
                            mystream.Flush();                      //释放网络流资源

                            btn_connect.Enabled = true;            //使能连接按钮
                            btn_disconnect.Enabled = false;        //禁用断开连接按钮
                            btn_open.Enabled = false;              //禁用打开按钮
                            btn_boot.Enabled = false;              //禁用进入BootLoader按钮
                            btn_send.Enabled = false;              //禁用发送/下载按钮

                            thread_recdata.Abort();        //终止数据接收线程
                        }
                        else if (datarec[0] == 0xaa & datarec[1] == 0xbb & datarec[2] == 0xcc & datarec[3] == 0xdd & datarec[4] == 0xee)    //判断终端是否成功进入BootLoader
                        {
                            MessageBox.Show("已经进入BootLoader");
                        }
                    }
                }        
            }   
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)     //窗体关闭时释放相应资源

        {
            try
            {
                myclient.Close();                                                    //关闭TcpClient
                mystream.Flush();  
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
                                                             //释放流资源
        }

        private void button2_Click_1(object sender, EventArgs e)                 //连接到终端
        {
            myendpoint = new IPEndPoint(IPAddress.Parse(txtlocalip.Text), 0);          //得到本机的IP地址及随机分配一个可用的端口号
            //（端口0 标识随机得到一个可用的端口号） 
            myclient = new TcpClient(myendpoint);                //实例化myclient
            if (myclient== null)
            {
                try
                {

                    myclient = new TcpClient(myendpoint);                       //若TcpClient为实例化，则实例化
                }
                catch (Exception ex)
                    {
                        Console.WriteLine(ex);                                 //若发生错误则输出错误信息
                    }
            }

            if (myclient.Connected != true)
            {
                try
                {
                    myclient.Connect(txtip.Text, int.Parse(port.Text));            //若处于为连接状态则连接
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.ToString());
                    MessageBox.Show("请检查网络链接");
                }
               
            }
          
            mystream = myclient.GetStream();                                   //建立用于传输网络数据的数据流

            thread_recdata = new Thread(new ThreadStart(receive_data));        //建立数据接收线程
            thread_recdata.Start();                                            //启动数据接收线程

            btn_connect.Enabled = false;                                       //禁用连接按钮
            btn_disconnect.Enabled = true;                                     //使能断开连接按钮
            btn_boot.Enabled = true;                                           //使能进入BootLoader按钮
            btn_open.Enabled = true;                                           //使能选择固件按钮
        }

        private void button3_Click(object sender, EventArgs e)                 //断开连接  释放相应的资源为下次连接座准备
        {
            thread_recdata.Abort();                                            //关闭数据接收线程
            myclient.Close();                                                  //关闭TcpClient
            mystream.Flush();                                                  //释放用于传输流
            myclient = null;                                                   //释放TcpClient
            btn_connect.Enabled = true;                                        //使能连接按钮
            btn_disconnect.Enabled = false;                                    //禁用断开连接按钮
            btn_boot.Enabled = false;
            btn_send.Enabled = false;
            btn_open.Enabled = false;
           
        }
    }
}
