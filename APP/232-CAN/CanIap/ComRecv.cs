﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UART_PORTS
{
    class ComRecv_Class
    {
        public UInt16 MaxLength = 60000;
        public UInt16 MinLen = 30;
        public UInt16 pWrite = 0;
        public UInt16 pRead = 0;
        public byte[] ComRecvBuf = new byte[60000];  // 这个1024是和MaxLength是一样值的。
    }
}
