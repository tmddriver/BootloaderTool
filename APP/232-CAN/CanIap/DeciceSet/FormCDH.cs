﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CanIAP.DeciceSet
{

    public partial class FormCDH : Form
    {
        string DeviceID;
        int DeviecTimeOut = 2000;//读写超时时间 单位ms
        string[] OutMode = new string[] { "定时输出", "变化+超时", "无输出" };
        public FormCDH(string deviceID, string canid)
        {
            InitializeComponent();
           
            DeviceID = deviceID;
            textBoxCanID.Text = canid;
            comboBoxMode.SelectedIndex = 0;

            //读取所有配置
            Thread thread = new Thread(new ThreadStart(FunReadAll));
            thread.IsBackground = true;
            thread.Start();
        }
        //读取所有配置
        private void FunReadAll()
        {
            //输出模式
            FunReadOutMode();
            //ID
            FunReadId();
            //阈值
            FunReadThreshold();
            //定时时间
            FunReadTime();
            //超时时间
            FunReadTimeOut();
        }

        #region 写入配置
        // 设置ID
        private void buttonSetID_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunSetId));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunSetId()
        {
            //判断ID是否为1-100的数值
            string patterns = @"^+?(([1-9]\d?)|(100)|(1))$";
            Regex regex = new Regex(patterns);
            if (regex.Match(textBoxCanID.Text).Success)
            {
                //修改ID
                byte[] Data = new byte[8];
                Data[0] = 0x01;
                Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
                Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
                Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
                Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);
                Data[5] = byte.Parse(regex.Match(textBoxCanID.Text).Value);
                Data[6] = 0;
                Data[7] = 0;

                //清除设置响应ID
                Form1.form1.SetResposeObj.ID = 0;
                Form1.form1.Send(0x78, 8, Data);
                //打印消息
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Green, ">修改ID为：{0}", Data[5]);
                Form1.form1.richbox.Msg(Color.Green, ">发送:[0X78] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

                Library.Time time = new Library.Time();
                Int64 nowtime = time.NowTimeMS();
                while ((Form1.form1.SetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
                {
                    Thread.Sleep(10);
                }
                if (time.NowTimeMS() - nowtime > DeviecTimeOut)
                {
                    MessageBox.Show("设备响应超时！");
                }
                else
                if (Form1.form1.SetResposeObj.Data[0] == 0x81)
                {
                    Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                    if (Form1.form1.SetResposeObj.Data[5] == 1)
                    {
                        Form1.form1.richbox.Msg(Color.Green, ">{0}", "设置成功!");
                        Form1.form1.ScanDevice();
                    }
                    else
                        Form1.form1.richbox.Msg(Color.Red, ">{0}", "设置失败！");
                }
                else
                {
                    Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                    Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
                }
            }
            else
                MessageBox.Show("请输入1-100的数值！");
        }

        //设置阈值
        private void button1_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunSetThreshold));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunSetThreshold()
        {
            //判断ID是否为1-100的数值
            string patterns = @"^+?(([1-9]\d?)|(100)|(1))$";
            Regex regex = new Regex(patterns);
            if (regex.Match(textBoxThreshold.Text).Success)
            {
                //设置阈值
                byte[] Data = new byte[8];
                Data[0] = 0x02;
                Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
                Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
                Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
                Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);
                Data[5] = byte.Parse(regex.Match(textBoxThreshold.Text).Value);
                Data[6] = 0;
                Data[7] = 0;

                //清除设置响应ID
                Form1.form1.SetResposeObj.ID = 0;
                Form1.form1.Send(0x78, 8, Data);
                //打印消息
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Green, ">设置阈值为：{0}", Data[5]);
                Form1.form1.richbox.Msg(Color.Green, ">发送:[0X78] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

                Library.Time time = new Library.Time();
                Int64 nowtime = time.NowTimeMS();
                while ((Form1.form1.SetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
                {
                    Thread.Sleep(10);
                }
                if (time.NowTimeMS() - nowtime > DeviecTimeOut)
                {
                    MessageBox.Show("设备响应超时！");
                }
                else
                if (Form1.form1.SetResposeObj.Data[0] == 0x82)
                {
                    Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                    if (Form1.form1.SetResposeObj.Data[5] == 1)
                        Form1.form1.richbox.Msg(Color.Green, ">{0}", "设置成功!");
                    else
                        Form1.form1.richbox.Msg(Color.Red, ">{0}", "设置失败！");
                }
                else
                {
                    Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                    Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
                }

            }
            else
                MessageBox.Show("请输入1-100的数值！");
        }
        //重置AD
        private void button2_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunResetAD));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunResetAD()
        {

            byte[] Data = new byte[8];
            Data[0] = 0x03;//重置AD
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);
            Data[5] = 0x01;
            Data[6] = 0;
            Data[7] = 0;

            //清除设置响应ID
            Form1.form1.SetResposeObj.ID = 0;
            Form1.form1.Send(0x78, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">重置AD！");
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X78] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.SetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.SetResposeObj.Data[0] == 0x83)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                if (Form1.form1.SetResposeObj.Data[5] == 1)
                    Form1.form1.richbox.Msg(Color.Green, ">{0}", "设置成功!");
                else
                    Form1.form1.richbox.Msg(Color.Red, ">{0}", "设置失败！");
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        //输出模式
        private void button3_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunSelectMode));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunSelectMode()
        {

            //输出模式
            byte[] Data = new byte[8];
            Data[0] = 0x04;
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);
            Data[5] = (byte)comboBoxMode.SelectedIndex;
            Data[6] = 0;
            Data[7] = 0;

            //清除设置响应ID
            Form1.form1.SetResposeObj.ID = 0;
            Form1.form1.Send(0x78, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">输出模式为：{0}", comboBoxMode.Items[Data[5]].ToString());
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X78] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.SetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.SetResposeObj.Data[0] == 0x84)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                if (Form1.form1.SetResposeObj.Data[5] == 1)
                    Form1.form1.richbox.Msg(Color.Green, ">{0}", "设置成功!");
                else
                    Form1.form1.richbox.Msg(Color.Red, ">{0}", "设置失败！");
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        //更新时间
        private void button4_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunSetTime));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunSetTime()
        {
            Int16 value;
            try
            {
                value = Int16.Parse(textBoxTime.Text.Trim());
                if (value > 5000 || value < 1)
                {
                    MessageBox.Show("请输入1-5000的数值！");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("请输入1-5000的数值！");
                return;
            }
            byte[] Data = new byte[8];
            Data[0] = 0x05;//更新时间
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);
            Data[5] = (byte)(value >> 8);
            Data[6] = (byte)(value);
            Data[7] = 0;

            //清除设置响应ID
            Form1.form1.SetResposeObj.ID = 0;
            Form1.form1.Send(0x78, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">定时输出时间为：{0}", value);
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X78] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.SetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.SetResposeObj.Data[0] == 0x85)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                if (Form1.form1.SetResposeObj.Data[5] == 1)
                    Form1.form1.richbox.Msg(Color.Green, ">{0}", "设置成功!");
                else
                    Form1.form1.richbox.Msg(Color.Red, ">{0}", "设置失败！");
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        //超时时间
        private void button5_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunSetTimeOut));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunSetTimeOut()
        {
            Int16 value;
            try
            {
                value = Int16.Parse(textBoxTimeOut.Text.Trim());
                if (value > 5000 || value < 1)
                {
                    MessageBox.Show("请输入1-5000的数值！");
                    return;
                }
            }
            catch
            {
                MessageBox.Show("请输入1-5000的数值！");
                return;
            }
            byte[] Data = new byte[8];
            Data[0] = 0x06;//更新超时时间
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);
            Data[5] = (byte)(value >> 8);
            Data[6] = (byte)(value);
            Data[7] = 0;

            //清除设置响应ID
            Form1.form1.SetResposeObj.ID = 0;
            Form1.form1.Send(0x78, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">变化+定时时间为：{0}", value);
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X78] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.SetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.SetResposeObj.Data[0] == 0x86)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                if (Form1.form1.SetResposeObj.Data[5] == 1)
                    Form1.form1.richbox.Msg(Color.Green, ">{0}", "设置成功!");
                else
                    Form1.form1.richbox.Msg(Color.Red, ">{0}", "设置失败！");
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        #endregion
        #region 读取配置
        //输出模式
        private void button8_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunReadOutMode));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunReadOutMode()
        {
            byte[] Data = new byte[8];
            Data[0] = 0x04;
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);

            //清除设置响应ID
            Form1.form1.GetResposeObj.ID = 0;
            Form1.form1.Send(0x7A, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">读取输出模式！");
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X7A] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.GetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.GetResposeObj.Data[0] == 0x94)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Fuchsia, ">输出模式:{0}", OutMode[Form1.form1.GetResposeObj.Data[5]]);
                comboBoxMode.SelectedIndex = Form1.form1.GetResposeObj.Data[5];
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        //ID
        private void button10_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunReadId));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunReadId()
        {
            byte[] Data = new byte[8];
            Data[0] = 0x01;
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);

            //清除设置响应ID
            Form1.form1.GetResposeObj.ID = 0;
            Form1.form1.Send(0x7A, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">读取ID！");
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X7A] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.GetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.GetResposeObj.Data[0] == 0x91)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Fuchsia, ">ID:{0}", Form1.form1.GetResposeObj.Data[5]);
                //更新到界面
                textBoxCanID.Text = Form1.form1.GetResposeObj.Data[5].ToString();
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        //阈值
        private void button9_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunReadThreshold));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunReadThreshold()
        {
            byte[] Data = new byte[8];
            Data[0] = 0x02;
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);

            //清除设置响应ID
            Form1.form1.GetResposeObj.ID = 0;
            Form1.form1.Send(0x7A, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">读取阈值！");
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X7A] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.GetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.GetResposeObj.Data[0] == 0x92)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Fuchsia, ">阈值:{0}", Form1.form1.GetResposeObj.Data[5]);
                //更新到界面
                textBoxThreshold.Text = Form1.form1.GetResposeObj.Data[5].ToString();
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        //定时时间
        private void button7_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunReadTime));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunReadTime()
        {
            byte[] Data = new byte[8];
            Data[0] = 0x05;
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);

            //清除设置响应ID
            Form1.form1.GetResposeObj.ID = 0;
            Form1.form1.Send(0x7A, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">读取定时时间！");
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X7A] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.GetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.GetResposeObj.Data[0] == 0x95)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Fuchsia, ">定时时间:{0}", Form1.form1.GetResposeObj.Data[5] << 8 | Form1.form1.GetResposeObj.Data[6]);
                //更新到界面
                textBoxTime.Text = (Form1.form1.GetResposeObj.Data[5] << 8 | Form1.form1.GetResposeObj.Data[6]).ToString();
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        //超时时间
        private void button6_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunReadTimeOut));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunReadTimeOut()
        {
            byte[] Data = new byte[8];
            Data[0] = 0x06;
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);

            //清除设置响应ID
            Form1.form1.GetResposeObj.ID = 0;
            Form1.form1.Send(0x7A, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">读取超时时间！");
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X7A] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.GetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.GetResposeObj.Data[0] == 0x96)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Fuchsia, ">超时时间:{0}", Form1.form1.GetResposeObj.Data[5] << 8 | Form1.form1.GetResposeObj.Data[6]);
                //更新到界面
                textBoxTimeOut.Text = (Form1.form1.GetResposeObj.Data[5] << 8 | Form1.form1.GetResposeObj.Data[6]).ToString();
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }
        #endregion
        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
