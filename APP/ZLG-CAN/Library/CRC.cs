﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Library
{
    public class CRC
    {
        private UInt32[] CRC32Table = new UInt32[256];
        public CRC()
        {
            CRC32TableCreate();
        }
        private void CRC32TableCreate()
        {
            uint c;
            uint i, j;

            for (i = 0; i < 256; i++)
            {
                c = (uint)i;
                for (j = 0; j < 8; j++)
                {
                    if ((c & 1) > 0)
                        c = (uint)0xedb88320L ^ (c >> 1);
                    else
                        c = c >> 1;
                }
                CRC32Table[i] = c;
            }

        }
        public UInt32 CRC32_U32(byte[] pBuf, int pBufSize)
        {
            UInt32 retCRCValue = 0xffffffff;
            while ((pBufSize--) > 0)
            {
                for (int i = 0; i < pBufSize; i++)
                {
                    retCRCValue = CRC32Table[(retCRCValue ^ pBuf[i]) & 0xFF] ^ (retCRCValue >> 8);
                }
            }
            return retCRCValue ^ 0xffffffff;
        }

        public byte[] CRC32_byte(byte[] pBuf, int pBufSize)
        {
            UInt32 retCRCValue = 0xffffffff;

            for (int i = 0; i < pBufSize; i++)
            {
                retCRCValue = CRC32Table[(retCRCValue ^ pBuf[i]) & 0xFF] ^ (retCRCValue >> 8);
            }

            retCRCValue = retCRCValue ^ 0xffffffff;
            byte[] value = new byte[4];
            value[0] = (byte)((retCRCValue >> 24) & 0xff);
            value[1] = (byte)((retCRCValue >> 16) & 0xff);
            value[2] = (byte)((retCRCValue >> 8) & 0xff);
            value[3] = (byte)((retCRCValue >> 0) & 0xff);
            return value;

        }
    }
}
