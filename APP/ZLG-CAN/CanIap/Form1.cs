﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
//using System.Drawing;
using System.Text;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using System.IO;
using System.Threading;
using System.Drawing;
using DispatchSystem;
using CanIAP.DeciceSet;



//1.ZLGCAN系列接口卡信息的数据类型。
public struct VCI_BOARD_INFO
{
    public UInt16 hw_Version;
    public UInt16 fw_Version;
    public UInt16 dr_Version;
    public UInt16 in_Version;
    public UInt16 irq_Num;
    public byte can_Num;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)] public byte[] str_Serial_Num;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 40)]
    public byte[] str_hw_Type;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
    public byte[] Reserved;
}


/////////////////////////////////////////////////////
//2.定义CAN信息帧的数据类型。
unsafe public struct VCI_CAN_OBJ  //使用不安全代码
{
    public uint ID;
    public uint TimeStamp;
    public byte TimeFlag;
    public byte SendType;
    public byte RemoteFlag;//是否是远程帧
    public byte ExternFlag;//是否是扩展帧
    public byte DataLen;

    public fixed byte Data[8];

    public fixed byte Reserved[3];

}
////2.定义CAN信息帧的数据类型。
//public struct VCI_CAN_OBJ 
//{
//    public UInt32 ID;
//    public UInt32 TimeStamp;
//    public byte TimeFlag;
//    public byte SendType;
//    public byte RemoteFlag;//是否是远程帧
//    public byte ExternFlag;//是否是扩展帧
//    public byte DataLen;
//    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 8)]
//    public byte[] Data;
//    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
//    public byte[] Reserved;

//    public void Init()
//    {
//        Data = new byte[8];
//        Reserved = new byte[3];
//    }
//}

//3.定义CAN控制器状态的数据类型。
public struct VCI_CAN_STATUS
{
    public byte ErrInterrupt;
    public byte regMode;
    public byte regStatus;
    public byte regALCapture;
    public byte regECCapture;
    public byte regEWLimit;
    public byte regRECounter;
    public byte regTECounter;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 4)]
    public byte[] Reserved;
}

//4.定义错误信息的数据类型。
public struct VCI_ERR_INFO
{
    public UInt32 ErrCode;
    public byte Passive_ErrData1;
    public byte Passive_ErrData2;
    public byte Passive_ErrData3;
    public byte ArLost_ErrData;
}

//5.定义初始化CAN的数据类型
public struct VCI_INIT_CONFIG
{
    public UInt32 AccCode;
    public UInt32 AccMask;
    public UInt32 Reserved;
    public byte Filter;
    public byte Timing0;
    public byte Timing1;
    public byte Mode;
}

public struct CHGDESIPANDPORT
{
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 10)]
    public byte[] szpwd;
    [MarshalAs(UnmanagedType.ByValArray, SizeConst = 20)]
    public byte[] szdesip;
    public Int32 desport;

    public void Init()
    {
        szpwd = new byte[10];
        szdesip = new byte[20];
    }
}

namespace CanIAP
{
    public partial class Form1 : Form
    {
        public static Form1 form1;

        #region CAN
        const int VCI_PCI5121 = 1;
        const int VCI_PCI9810 = 2;
        const int VCI_USBCAN1 = 3;
        const int VCI_USBCAN2 = 4;
        const int VCI_USBCAN2A = 4;
        const int VCI_PCI9820 = 5;
        const int VCI_CAN232 = 6;
        const int VCI_PCI5110 = 7;
        const int VCI_CANLITE = 8;
        const int VCI_ISA9620 = 9;
        const int VCI_ISA5420 = 10;
        const int VCI_PC104CAN = 11;
        const int VCI_CANETUDP = 12;
        const int VCI_CANETE = 12;
        const int VCI_DNP9810 = 13;
        const int VCI_PCI9840 = 14;
        const int VCI_PC104CAN2 = 15;
        const int VCI_PCI9820I = 16;
        const int VCI_CANETTCP = 17;
        const int VCI_PEC9920 = 18;
        const int VCI_PCI5010U = 19;
        const int VCI_USBCAN_E_U = 20;
        const int VCI_USBCAN_2E_U = 21;
        const int VCI_PCI5020U = 22;
        const int VCI_EG20T_CAN = 23;
        const int VCI_PCIE9120I = 27;
        const int VCI_PCIE9110I = 28;
        const int VCI_PCIE9140I = 29;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="DeviceType"></param>
        /// <param name="DeviceInd"></param>
        /// <param name="Reserved"></param>
        /// <returns></returns>
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_OpenDevice(UInt32 DeviceType, UInt32 DeviceInd, UInt32 Reserved);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_CloseDevice(UInt32 DeviceType, UInt32 DeviceInd);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_InitCAN(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, ref VCI_INIT_CONFIG pInitConfig);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_ReadBoardInfo(UInt32 DeviceType, UInt32 DeviceInd, ref VCI_BOARD_INFO pInfo);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_ReadErrInfo(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, ref VCI_ERR_INFO pErrInfo);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_ReadCANStatus(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, ref VCI_CAN_STATUS pCANStatus);

        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_GetReference(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, UInt32 RefType, ref byte pData);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_SetReference(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, UInt32 RefType, ref byte pData);

        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_GetReceiveNum(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_ClearBuffer(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd);

        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_StartCAN(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd);
        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_ResetCAN(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd);

        [DllImport("controlcan.dll")]
        static extern UInt32 VCI_Transmit(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, ref VCI_CAN_OBJ pSend, UInt32 Len);

        //[DllImport("controlcan.dll")]
        //static extern UInt32 VCI_Receive(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, ref VCI_CAN_OBJ pReceive, UInt32 Len, Int32 WaitTime);
        [DllImport("controlcan.dll", CharSet = CharSet.Ansi)]
        static extern UInt32 VCI_Receive(UInt32 DeviceType, UInt32 DeviceInd, UInt32 CANInd, IntPtr pReceive, UInt32 Len, Int32 WaitTime);

        static UInt32 m_devtype = 4;//USBCAN2

        UInt32 m_bOpen = 0;
        UInt32 m_devind = 0;
        UInt32 m_canind = 0;

        VCI_CAN_OBJ[] m_recobj = new VCI_CAN_OBJ[50];

        UInt32[] m_arrdevtype = new UInt32[20];
        #endregion

        #region Bootloader
        byte[] databuf;
        byte[] datatosend = new byte[1024];                      //定义数据发送缓冲区
        byte[] datarec = new byte[1024];                         //定义数据发送缓冲区
        int packet_zheng = 0;                                    //定义固件大小的整K字节数
        int packet_yu = 0;                                       //定义固件大小的不足1K字节个数
        /// <summary>
        /// 已经发送整K字节个数
        /// </summary>
        int packet_send = 0;
        public Library.Richbox richbox;                              //固件下载消息框
        Library.CRC CRC = new Library.CRC();
        #endregion

        public VCI_CAN_OBJ SetResposeObj;//设置响应对象
        public VCI_CAN_OBJ GetResposeObj;//读取响应对象

        public Form1()
        {
            InitializeComponent();
            form1 = this;
            Form1.CheckForIllegalCrossThreadCalls = false;       //可跨线程使用控件
            btn_send.Enabled = false;                            //禁用发送/下载按钮
        }

        string[] DevicelistKey = new string[6];
        string[] DeviceType = new string[] { "未定义", "主板", "磁导航-8", "磁导航-16", "地标", "超声波", "遥控器" };
        string[] RunMode = new string[] { "", "BootLoader", "App" };
        private void Form1_Load(object sender, EventArgs e)
        {
            #region CAN
            comboBox_DevIndex.SelectedIndex = 0;
            comboBox_CANIndex.SelectedIndex = 0;
            textBox_AccCode.Text = "00000000";
            textBox_AccMask.Text = "FFFFFFFF";
            textBox_Time0.Text = "00";
            textBox_Time1.Text = "1C";
            comboBox_Filter.SelectedIndex = 1;
            comboBox_Mode.SelectedIndex = 0;
            comboBox_SendType.SelectedIndex = 0;
            comboBox_FrameFormat.SelectedIndex = 0;
            comboBox_FrameType.SelectedIndex = 0;
            textBox_ID.Text = "7B";
            textBox_Data.Text = "81 01 02 03 04 21 03 01";

            //
            Int32 curindex = 0;
            comboBox_devtype.Items.Clear();

            curindex = comboBox_devtype.Items.Add("VCI_PCI5121");
            m_arrdevtype[curindex] = VCI_PCI5121;
            //comboBox_devtype.Items[0] = "VCI_PCI5121";
            //m_arrdevtype[0]=  VCI_PCI5121 ;

            curindex = comboBox_devtype.Items.Add("VCI_PCI9810");
            m_arrdevtype[curindex] = VCI_PCI9810;
            //comboBox_devtype.Items[1] = "VCI_PCI9810";
            //m_arrdevtype[1]=  VCI_PCI9810 ;

            curindex = comboBox_devtype.Items.Add("VCI_USBCAN1(I+)");
            m_arrdevtype[curindex] = VCI_USBCAN1;
            //comboBox_devtype.Items[2] = "VCI_USBCAN1";
            //m_arrdevtype[2]=  VCI_USBCAN1 ;

            curindex = comboBox_devtype.Items.Add("VCI_USBCAN2(II+)");
            m_arrdevtype[curindex] = VCI_USBCAN2;
            //comboBox_devtype.Items[3] = "VCI_USBCAN2";
            //m_arrdevtype[3]=  VCI_USBCAN2 ;

            curindex = comboBox_devtype.Items.Add("VCI_USBCAN2A");
            m_arrdevtype[curindex] = VCI_USBCAN2A;
            //comboBox_devtype.Items[4] = "VCI_USBCAN2A";
            //m_arrdevtype[4]=  VCI_USBCAN2A ;

            curindex = comboBox_devtype.Items.Add("VCI_PCI9820");
            m_arrdevtype[curindex] = VCI_PCI9820;
            //comboBox_devtype.Items[5] = "VCI_PCI9820";
            //m_arrdevtype[5]=  VCI_PCI9820 ;

            curindex = comboBox_devtype.Items.Add("VCI_PCI5110");
            m_arrdevtype[curindex] = VCI_PCI5110;
            //comboBox_devtype.Items[6] = "VCI_PCI5110";
            //m_arrdevtype[6]=  VCI_PCI5110 ;

            curindex = comboBox_devtype.Items.Add("VCI_CANLITE");
            m_arrdevtype[curindex] = VCI_CANLITE;

            curindex = comboBox_devtype.Items.Add("VCI_ISA9620");
            m_arrdevtype[curindex] = VCI_ISA9620;
            //comboBox_devtype.Items[7] = "VCI_ISA9620";
            //m_arrdevtype[7]=  VCI_ISA9620 ;

            curindex = comboBox_devtype.Items.Add("VCI_ISA5420");
            m_arrdevtype[curindex] = VCI_ISA5420;
            //comboBox_devtype.Items[8] = "VCI_ISA5420";
            //m_arrdevtype[8]=  VCI_ISA5420 ;

            curindex = comboBox_devtype.Items.Add("VCI_PC104CAN");
            m_arrdevtype[curindex] = VCI_PC104CAN;
            //comboBox_devtype.Items[9] = "VCI_PC104CAN";
            //m_arrdevtype[9]=  VCI_PC104CAN ;

            curindex = comboBox_devtype.Items.Add("VCI_DNP9810");
            m_arrdevtype[curindex] = VCI_DNP9810;
            //comboBox_devtype.Items[10] = "VCI_DNP9810";
            //m_arrdevtype[10]=  VCI_DNP9810 ;

            curindex = comboBox_devtype.Items.Add("VCI_PCI9840");
            m_arrdevtype[curindex] = VCI_PCI9840;
            //comboBox_devtype.Items[11] = "VCI_PCI9840";
            //m_arrdevtype[11]=   VCI_PCI9840;

            curindex = comboBox_devtype.Items.Add("VCI_PC104CAN2");
            m_arrdevtype[curindex] = VCI_PC104CAN2;
            //comboBox_devtype.Items[12] = "VCI_PC104CAN2";
            //m_arrdevtype[12]=  VCI_PC104CAN2 ;

            curindex = comboBox_devtype.Items.Add("VCI_PCI9820I");
            m_arrdevtype[curindex] = VCI_PCI9820I;
            //comboBox_devtype.Items[13] = "VCI_PCI9820I";
            //m_arrdevtype[13]=  VCI_PCI9820I ;

            curindex = comboBox_devtype.Items.Add("VCI_PEC9920");
            m_arrdevtype[curindex] = VCI_PEC9920;
            //comboBox_devtype.Items[14] = "VCI_PEC9920";
            //m_arrdevtype[14]= VCI_PEC9920  ;

            curindex = comboBox_devtype.Items.Add("VCI_PCIE9120I");
            m_arrdevtype[curindex] = VCI_PCIE9120I;

            curindex = comboBox_devtype.Items.Add("VCI_PCIE9110I");
            m_arrdevtype[curindex] = VCI_PCIE9110I;

            curindex = comboBox_devtype.Items.Add("VCI_PCIE9140I");
            m_arrdevtype[curindex] = VCI_PCIE9140I;

            comboBox_devtype.SelectedIndex = 2;
            comboBox_devtype.MaxDropDownItems = comboBox_devtype.Items.Count;
            #endregion

            #region 扫描设备列表初始化
            DevicelistKey[0] = "序号";
            DevicelistKey[1] = "芯片ID";
            DevicelistKey[2] = "CAN-ID";
            DevicelistKey[3] = "设备类型";
            DevicelistKey[4] = "运行模式";

            exListView1.FullRowSelect = true;//要选择就是一行
            exListView1.Columns.Add(DevicelistKey[0], 50, HorizontalAlignment.Center);
            exListView1.Columns.Add(DevicelistKey[1], 120, HorizontalAlignment.Center);
            exListView1.Columns.Add(DevicelistKey[2], 80, HorizontalAlignment.Center);
            exListView1.Columns.Add(DevicelistKey[3], 130, HorizontalAlignment.Center);
            exListView1.Columns.Add(DevicelistKey[4], -2, HorizontalAlignment.Left);
            #endregion

            richbox = new Library.Richbox(textBoxMsg);

            byte[] Data = new byte[8];
            for (int i = 0; i < 8; i++)
            {
                Data[i] = 0x00;
            }
            //计算CRC
            Data = CRC.CRC32_byte(Data, 8);
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {
            if (m_bOpen == 1)
            {
                VCI_CloseDevice(m_devtype, m_devind);
            }
        }

        private void buttonConnect_Click(object sender, EventArgs e)
        {
            if (m_bOpen == 1)
            {
                VCI_CloseDevice(m_devtype, m_devind);
                m_bOpen = 0;
            }
            else
            {
                m_devtype = m_arrdevtype[comboBox_devtype.SelectedIndex];

                m_devind = (UInt32)comboBox_DevIndex.SelectedIndex;
                m_canind = (UInt32)comboBox_CANIndex.SelectedIndex;
                if (VCI_OpenDevice(m_devtype, m_devind, 0) == 0)
                {
                    MessageBox.Show("打开设备失败,请检查设备类型和设备索引号是否正确", "错误",
                            MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

                m_bOpen = 1;
                VCI_INIT_CONFIG config = new VCI_INIT_CONFIG();
                config.AccCode = System.Convert.ToUInt32("0x" + textBox_AccCode.Text, 16);
                config.AccMask = System.Convert.ToUInt32("0x" + textBox_AccMask.Text, 16);
                config.Timing0 = System.Convert.ToByte("0x" + textBox_Time0.Text, 16);
                config.Timing1 = System.Convert.ToByte("0x" + textBox_Time1.Text, 16);
                config.Filter = (Byte)comboBox_Filter.SelectedIndex;
                config.Mode = (Byte)comboBox_Mode.SelectedIndex;
                VCI_InitCAN(m_devtype, m_devind, m_canind, ref config);

                //启动
                VCI_StartCAN(m_devtype, m_devind, m_canind);
            }
            buttonConnect.Text = m_bOpen == 1 ? "断开" : "连接";
            timer_rec.Enabled = m_bOpen == 1 ? true : false;
        }

        unsafe private void timer_rec_Tick(object sender, EventArgs e)
        {
            UInt32 res = new UInt32();
            res = VCI_GetReceiveNum(m_devtype, m_devind, m_canind);
            if (res == 0)
                return;
            //res = VCI_Receive(m_devtype, m_devind, m_canind, ref m_recobj[0],50, 100);

            /////////////////////////////////////
            UInt32 con_maxlen = 50;
            IntPtr pt = Marshal.AllocHGlobal(Marshal.SizeOf(typeof(VCI_CAN_OBJ)) * (Int32)con_maxlen);

            res = VCI_Receive(m_devtype, m_devind, m_canind, pt, con_maxlen, 100);
            ////////////////////////////////////////////////////////

            String str = "";
            for (UInt32 i = 0; i < res; i++)
            {
                VCI_CAN_OBJ obj = (VCI_CAN_OBJ)Marshal.PtrToStructure((IntPtr)((UInt32)pt + i * Marshal.SizeOf(typeof(VCI_CAN_OBJ))), typeof(VCI_CAN_OBJ));

                str = "接收到数据: ";
                str += "  帧ID:0x" + System.Convert.ToString((Int32)obj.ID, 16);
                str += "  帧格式:";

                if (obj.RemoteFlag == 0)
                    str += "数据帧 ";
                else
                    str += "远程帧 ";

                if (obj.ExternFlag == 0)
                    str += "标准帧 ";
                else
                    str += "扩展帧 ";

                //////////////////////////////////////////
                if (obj.RemoteFlag == 0)
                {
                    str += "数据: ";
                    byte len = (byte)(obj.DataLen % 9);
                    byte j = 0;
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[0]);
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[1]);
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[2]);
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[3]);
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[4]);
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[5]);
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[6]);
                    if (j++ < len)
                        str += " " + String.Format("{0:X2}", obj.Data[7]);

                }

                listBox_Info.Items.Add(str);
                listBox_Info.SelectedIndex = listBox_Info.Items.Count - 1;


                #region 扫描列表监控
                //当前为数据帧且为标准帧
                if (obj.RemoteFlag == 0 && obj.ExternFlag == 0)
                {
                    //读设备信息响应
                    if (obj.ID == 0x7B)
                    {
                        switch (obj.Data[0])
                        {
                            case 0x81://读设备信息响应
                                //解析
                                int serialNum = (obj.Data[1] << 24) + (obj.Data[2] << 16) + (obj.Data[3] << 8) + obj.Data[4];
                                int canID = obj.Data[5];
                                int deviceType = obj.Data[6];
                                int runMode = obj.Data[7];
                                bool resault = false;
                                //搜索列表是否存在当前设备，存在更新，不存在添加
                                for (int j = 0; j < exListView1.Items.Count; j++)
                                {
                                    //根据芯片序列号判断
                                    if (exListView1.Items[j].SubItems[1].Text == String.Format("{0:X8}", serialNum))
                                    {
                                        //更新
                                        exListView1.Items[j].SubItems[2].Text = canID.ToString();
                                        exListView1.Items[j].SubItems[3].Text = DeviceType[deviceType];
                                        exListView1.Items[j].SubItems[4].Text = RunMode[runMode];
                                        resault = true;
                                        break;
                                    }
                                }
                                if (resault == false)
                                {
                                    //添加到列表
                                    ListViewItem item = new ListViewItem();
                                    item.Text = exListView1.Items.Count.ToString();//"序号";
                                    item.SubItems.Add(String.Format("{0:X8}", serialNum));//  "芯片ID";
                                    item.SubItems.Add(canID.ToString());//  "CAN-ID";
                                    item.SubItems.Add(DeviceType[deviceType]);//  "设备类型";
                                    item.SubItems.Add(RunMode[runMode]);//  "运行模式";

                                    exListView1.Items.Add(item);
                                    int index = exListView1.Items.Count - 1;
                                    exListView1.Items[index].BackColor = index % 2 == 0 ? Color.White : Color.FromArgb(0xf0, 0xf5, 0xf5, 0xf5);
                                    exListView1.EnsureVisible(exListView1.Items.Count - 1);//滚动到指定的行位置
                                }
                                break;
                            default:
                                break;
                        }
                    }
                    //请求刷固响应
                    else if (obj.ID == 0x7E)
                    {
                        //通知刷固件主进程
                        for (int j = 0; j < 8; j++)
                        {
                            ResponseData[j] = obj.Data[j];
                        }
                    }
                    //配置响应
                    else if (obj.ID == 0x79)
                    {
                        SetResposeObj = obj;
                    }
                    //查询响应
                    else if (obj.ID == 0x7C)
                    {
                        GetResposeObj = obj;
                    }

                }
                #endregion

            }
            Marshal.FreeHGlobal(pt);
        }

        private void button_StartCAN_Click(object sender, EventArgs e)
        {
            if (m_bOpen == 0)
                return;
            VCI_StartCAN(m_devtype, m_devind, m_canind);
        }

        private void button_StopCAN_Click(object sender, EventArgs e)
        {
            if (m_bOpen == 0)
                return;
            VCI_ResetCAN(m_devtype, m_devind, m_canind);
        }

        unsafe private void button_Send_Click(object sender, EventArgs e)
        {
            if (m_bOpen == 0)
                return;

            VCI_CAN_OBJ sendobj = new VCI_CAN_OBJ();
            //sendobj.Init();
            sendobj.SendType = (byte)comboBox_SendType.SelectedIndex;
            sendobj.RemoteFlag = (byte)comboBox_FrameFormat.SelectedIndex;
            sendobj.ExternFlag = (byte)comboBox_FrameType.SelectedIndex;
            sendobj.ID = System.Convert.ToUInt32("0x" + textBox_ID.Text, 16);
            int len = (textBox_Data.Text.Length + 1) / 3;
            sendobj.DataLen = System.Convert.ToByte(len);
            String strdata = textBox_Data.Text;
            int i = -1;
            if (i++ < len - 1)
                sendobj.Data[0] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);
            if (i++ < len - 1)
                sendobj.Data[1] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);
            if (i++ < len - 1)
                sendobj.Data[2] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);
            if (i++ < len - 1)
                sendobj.Data[3] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);
            if (i++ < len - 1)
                sendobj.Data[4] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);
            if (i++ < len - 1)
                sendobj.Data[5] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);
            if (i++ < len - 1)
                sendobj.Data[6] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);
            if (i++ < len - 1)
                sendobj.Data[7] = System.Convert.ToByte("0x" + strdata.Substring(i * 3, 2), 16);

            if (VCI_Transmit(m_devtype, m_devind, m_canind, ref sendobj, 1) == 0)
            {
                MessageBox.Show("发送失败", "错误",
                        MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        OpenFileDialog openfile = new OpenFileDialog();
        private void LoadFile()
        {
            string str = string.Empty;
            //判断文件是Bin还是Hex
            if (openfile.FileName.EndsWith(".bin"))
            {
                //获取文件流
                FileStream fs = new FileStream(openfile.FileName, FileMode.Open);
                databuf = new byte[fs.Length];
                fs.Read(databuf, 0, (int)fs.Length);
                fs.Close();
            }
            else
            if (openfile.FileName.EndsWith(".hex"))
            {
                Library.HexToBin hexToBin = new Library.HexToBin();
                //转换到bin并获取数组
                databuf = hexToBin.ToBinByte(openfile.FileName, 0x2000);
            }
            str = "文件共" + databuf.Length.ToString() + "字节";                      //获取文件的总字节数
            richbox.Msg(str);                                               //显示文件的总字节数
            packet_zheng = (int)databuf.Length / 1024;                                   //获取文件的整K字节数
            packet_yu = (int)databuf.Length % 1024;                                      //获取不足1K的剩余字节数
        }
        private void btn_open_Click(object sender, EventArgs e)
        {
            try
            {
                openfile.Filter = @"固件(*.hex) | *.hex|固件(*.bin) | *.bin";
                //显示打开文件对话框
                openfile.ShowDialog();
                //获取所选择固件的名称
                txt_filename.Text = openfile.FileName;
                LoadFile();

                btn_send.Enabled = true;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        byte[] ResponseData = new byte[8];//请求响应

        //正在刷固件
        bool Running = false;

        private void btn_send_Click(object sender, EventArgs e)
        {
            if (btn_send.Text == "下载")
            {
                LoadFile();
                Running = true;
                btn_send.Text = "取消";
                //启动刷固件进程
                Thread thread = new Thread(new ThreadStart(threadFun));
                thread.IsBackground = true;
                thread.Start();
            }
            else
            {
                Running = false;
                btn_send.Text = "下载";
                richbox.Msg(string.Format("已取消！"));
                btn_open.Enabled = true;
            }
        }

        int StartTimeOut = 10;//握手信号超时时间 ms
        int StartRepeat = 3;//握手信号超时次数 ms
        int timeout = 100000;//超时时间 ms
        int repeat = 3;//超时次数
        //刷固件进程
        private void threadFun()
        {
            //清空已发送计数
            packet_send = 0;
            int timeCount = 0;
            Library.Time time = new Library.Time();
            //获取启动时间
            long StartTime = time.NowTimeMS();
            while (Running)
            {
                //1.请求刷固
                for (int i = 0; i < StartRepeat; i++)
                {
                    richbox.Msg(Color.Green, "{0} 第{1}次请求刷固...", DateTime.Now.ToString("HH:mm:ss fff"), i + 1);
                    byte[] Data = new byte[8];
                    //目标芯片ID
                    Data[0] = 0x01;
                    Data[1] = Convert.ToByte(txttargetIC.Text.Substring(0, 2), 16);
                    Data[2] = Convert.ToByte(txttargetIC.Text.Substring(2, 2), 16);
                    Data[3] = Convert.ToByte(txttargetIC.Text.Substring(4, 2), 16);
                    Data[4] = Convert.ToByte(txttargetIC.Text.Substring(6, 2), 16);
                    Data[5] = Convert.ToByte(databuf.Length >> 16);
                    Data[6] = Convert.ToByte(databuf.Length >> 8);
                    Data[7] = Convert.ToByte(databuf.Length & 0xff);
                    Send(0x7F, 8, Data);
                    string str = string.Empty;
                    for (int j = 0; j < 8; j++)
                    {
                        str += String.Format("{0:X2}", Data[j]) + " ";
                    }
                    richbox.Msg(str);

                    //等待响应
                    while (Running)
                    {
                        if (timeCount * 10 > StartTimeOut)
                        {
                            if (i == StartRepeat - 1)
                            {
                                richbox.Msg(Color.Red, "{0} 等待超时，设备无响应，任务已取消！", DateTime.Now.ToString("HH:mm:ss fff"));
                                Running = false;
                                btn_send.Text = "下载";
                                goto stop;
                                break;
                            }
                            else
                                break;
                        }

                        if (ResponseData[0] == 0x81)
                        {
                            int serialNum = (ResponseData[1] << 24) + (ResponseData[2] << 16) + (ResponseData[3] << 8) + ResponseData[4];
                            //判断芯片ID
                            if (String.Format("{0:X8}", serialNum) == txttargetIC.Text)
                            {
                                Array.Clear(ResponseData, 0, ResponseData.Length);
                                goto AA;
                                break;
                            }
                        }
                        Thread.Sleep(10);
                        timeCount++;
                    }
                    if (Running == false)
                    {
                        goto stop;
                    }
                    timeCount = 0;
                }

                AA:
                //2.开始刷固
                //发送整K字节数据
                for (int i = 0; i < packet_zheng; i++)
                {
                    if (Running == false)
                        break;
                    richbox.Msg(Color.Blue, "{0} 已发送{1}字节", DateTime.Now.ToString("HH:mm:ss fff"), (i + 1) * 1024);
                    send_data_zheng();
                    //等待响应
                    timeCount = 0;
                    while (Running)
                    {
                        //超时退出
                        if (timeCount * 10 > timeout)
                        {
                            richbox.Msg(Color.Red, "{0} 等待超时，设备无响应，任务已取消！", DateTime.Now.ToString("HH:mm:ss fff"));
                            Running = false;
                            btn_send.Text = "下载";
                            goto stop;
                        }
                        if (ResponseData[0] == 0x82)
                        {
                            int serialNum = (ResponseData[1] << 24) + (ResponseData[2] << 16) + (ResponseData[3] << 8) + ResponseData[4];
                            int reciveLen = (ResponseData[5] << 16) + (ResponseData[6] << 8) + ResponseData[7];
                            //判断芯片ID
                            if (String.Format("{0:X8}", serialNum) == txttargetIC.Text)
                            {
                                //收发匹配
                                if (reciveLen == (i + 1) * 1024)
                                {
                                    //没有不足1k的数据，并且为最后一帧
                                    if ((packet_yu == 0) && (i == packet_zheng - 1))
                                    {
                                        Array.Clear(ResponseData, 0, ResponseData.Length);
                                        //等待刷固结果
                                        timeCount = 0;
                                        while (Running)
                                        {
                                            //超时退出
                                            if (timeCount * 10 > timeout)
                                            {
                                                richbox.Msg(Color.Red, "{0} 等待超时，设备无响应，任务已取消！", DateTime.Now.ToString("HH:mm:ss fff"));
                                                Running = false;
                                                btn_send.Text = "下载";
                                                goto stop;
                                            }
                                            if (ResponseData[0] == 0x84)
                                            {
                                                serialNum = (ResponseData[1] << 24) + (ResponseData[2] << 16) + (ResponseData[3] << 8) + ResponseData[4];
                                                reciveLen = (ResponseData[5] << 16) + (ResponseData[6] << 8) + ResponseData[7];
                                                //判断芯片ID
                                                if (String.Format("{0:X8}", serialNum) == txttargetIC.Text)
                                                {
                                                    if (ResponseData[5] == 0x01)
                                                    {
                                                        richbox.Msg(Color.Green, "{0} Flash写入成功！", DateTime.Now.ToString("HH:mm:ss fff"));
                                                        richbox.Msg(Color.Red, "完成！");
                                                    }
                                                    else if (ResponseData[5] == 0x02)
                                                    {
                                                        richbox.Msg(Color.Red, "{0} Flash写入失败！", DateTime.Now.ToString("HH:mm:ss fff"), 1024, reciveLen);
                                                        richbox.Msg(Color.Red, "任务取消！");
                                                    }

                                                    Running = false;
                                                    btn_send.Text = "下载";
                                                    goto stop;
                                                }
                                            }

                                            Thread.Sleep(10);
                                            timeCount++;
                                        }
                                    }
                                    else
                                    {
                                        richbox.Msg(Color.Orange, "{0} 设备已接收{1}字节", DateTime.Now.ToString("HH:mm:ss fff"), reciveLen);
                                        Array.Clear(ResponseData, 0, ResponseData.Length);
                                        break;
                                    }
                                }
                            }
                        }
                        else
                        if (ResponseData[0] == 0x83)
                        {
                            int serialNum = (ResponseData[1] << 24) + (ResponseData[2] << 16) + (ResponseData[3] << 8) + ResponseData[4];
                            int reciveLen = (ResponseData[5] << 16) + (ResponseData[6] << 8) + ResponseData[7];
                            //判断芯片ID
                            if (String.Format("{0:X8}", serialNum) == txttargetIC.Text)
                            {
                                if (ResponseData[5] == 0x01)
                                    richbox.Msg(Color.Red, "{0} 校验和错误！", DateTime.Now.ToString("HH:mm:ss fff"));
                                else if (ResponseData[5] == 0x02)
                                    richbox.Msg(Color.Red, "{0} 长度错误，发送{1}字节，接收{2}！", DateTime.Now.ToString("HH:mm:ss fff"), 1024, reciveLen);

                                richbox.Msg(Color.Red, "任务取消！");
                                Running = false;
                                btn_send.Text = "下载";
                                goto stop;
                            }
                        }
                        Thread.Sleep(10);
                        timeCount++;
                    }
                    //已发送整k数据计数
                    packet_send++;
                }

                //发送不足1K数据
                if (Running)
                {
                    send_data_yu();
                    richbox.Msg(Color.Blue, "{0} 已发送{1}字节", DateTime.Now.ToString("HH:mm:ss fff"), packet_zheng * 1024 + packet_yu);
                }
                //等待响应
                timeCount = 0;
                while (Running)
                {
                    //超时退出
                    if (timeCount * 10 > timeout)
                    {
                        richbox.Msg(Color.Red, "{0} 等待超时，设备无响应", DateTime.Now.ToString("HH:mm:ss fff"));
                        richbox.Msg(Color.Red, "任务取消！");
                        Running = false;
                        btn_send.Text = "下载";
                        goto stop;
                    }
                    if (ResponseData[0] == 0x82)
                    {
                        int serialNum = (ResponseData[1] << 24) + (ResponseData[2] << 16) + (ResponseData[3] << 8) + ResponseData[4];
                        int reciveLen = (ResponseData[5] << 16) + (ResponseData[6] << 8) + ResponseData[7];
                        //判断芯片ID
                        if (String.Format("{0:X8}", serialNum) == txttargetIC.Text)
                        {
                            if (reciveLen == databuf.Length)
                            {
                                richbox.Msg(Color.Orange, "{0} 设备已接收{1}字节", DateTime.Now.ToString("HH:mm:ss fff"), reciveLen);

                                Array.Clear(ResponseData, 0, ResponseData.Length);
                                //等待刷固结果
                                timeCount = 0;
                                while (Running)
                                {
                                    //超时退出
                                    if (timeCount * 10 > timeout)
                                    {
                                        richbox.Msg(Color.Red, "{0} 等待超时，设备无响应，任务已取消！", DateTime.Now.ToString("HH:mm:ss fff"));
                                        Running = false;
                                        btn_send.Text = "下载";
                                        goto stop;
                                    }
                                    if (ResponseData[0] == 0x84)
                                    {
                                        serialNum = (ResponseData[1] << 24) + (ResponseData[2] << 16) + (ResponseData[3] << 8) + ResponseData[4];
                                        reciveLen = (ResponseData[5] << 16) + (ResponseData[6] << 8) + ResponseData[7];
                                        //判断芯片ID
                                        if (String.Format("{0:X8}", serialNum) == txttargetIC.Text)
                                        {
                                            if (ResponseData[5] == 0x01)
                                            {
                                                richbox.Msg(Color.Green, "{0} Flash写入成功！", DateTime.Now.ToString("HH:mm:ss fff"));
                                                richbox.Msg(Color.Green, "完成！");
                                            }
                                            else if (ResponseData[5] == 0x02)
                                            {
                                                richbox.Msg(Color.Red, "{0} Flash写入失败！", DateTime.Now.ToString("HH:mm:ss fff"), 1024, reciveLen);
                                                richbox.Msg(Color.Red, "完成！");
                                            }

                                            Running = false;
                                            btn_send.Text = "下载";
                                            goto stop;
                                        }
                                    }

                                    Thread.Sleep(10);
                                    timeCount++;
                                }
                            }
                            //收发长度不匹配
                            else
                            {
                                richbox.Msg(Color.Red, "{0} 长度错误，发送{1}字节，接收{2}！", DateTime.Now.ToString("HH:mm:ss fff"), 1024, reciveLen);
                                richbox.Msg(Color.Red, "任务取消！");
                                Running = false;
                                btn_send.Text = "下载";
                                goto stop;
                            }
                        }
                        else
                        {


                        }
                    }
                    else
                    if (ResponseData[0] == 0x83)
                    {
                        int serialNum = (ResponseData[1] << 24) + (ResponseData[2] << 16) + (ResponseData[3] << 8) + ResponseData[4];
                        int reciveLen = (ResponseData[5] << 16) + (ResponseData[6] << 8) + ResponseData[7];
                        //判断芯片ID
                        if (String.Format("{0:X8}", serialNum) == txttargetIC.Text)
                        {
                            if (ResponseData[5] == 0x01)
                                richbox.Msg(Color.Red, "{0} 校验和错误！", DateTime.Now.ToString("HH:mm:ss fff"));
                            else if (ResponseData[5] == 0x02)
                                richbox.Msg(Color.Red, "{0} 长度错误，发送{1}字节，接收{2}！", DateTime.Now.ToString("HH:mm:ss fff"), packet_yu, reciveLen);
                            else if (ResponseData[5] == 0x03)
                                richbox.Msg(Color.Red, "{0} FLASH内无程序！", DateTime.Now.ToString("HH:mm:ss fff"));
                            richbox.Msg(Color.Red, "任务取消！");
                            Running = false;
                            btn_send.Text = "下载";
                            goto stop;
                        }
                    }
                }
            }
            stop:;
            //获取结束时间
            long StopTime = (DateTime.Now.ToUniversalTime().Ticks - 621355968000000000) / 10000;
            richbox.Msg(Color.Green, "耗时：{0}ms", StopTime - StartTime);
            Thread.Sleep(1100);
            ScanDevice();
        }
        /// <summary>
        /// 发送整K字节数据
        /// </summary>
        private void send_data_zheng()
        {
            byte[] Data = new byte[8];
            Array.Copy(databuf, packet_send * 1024, datatosend, 0, 1024);
            for (int i = 0; i < 128; i++)
            {
                Data[0] = datatosend[i * 8 + 0];
                Data[1] = datatosend[i * 8 + 1];
                Data[2] = datatosend[i * 8 + 2];
                Data[3] = datatosend[i * 8 + 3];
                Data[4] = datatosend[i * 8 + 4];
                Data[5] = datatosend[i * 8 + 5];
                Data[6] = datatosend[i * 8 + 6];
                Data[7] = datatosend[i * 8 + 7];
                Send(0x7F, 8, Data);
            }
            //计算CRC
            Data = CRC.CRC32_byte(datatosend, 1024);
            //发送校验
            Send(0x7F, Data.Length, Data);
        }
        /// <summary>
        /// 发送不足1K部分
        /// </summary>
        private void send_data_yu()                                                //发送不足1K字节数据
        {
            int len;
            byte[] Data = new byte[8];
            Array.Copy(databuf, packet_send * 1024, datatosend, 0, packet_yu);
            if (packet_yu % 8 > 0)
                len = packet_yu / 8 + 1;
            else
                len = packet_yu / 8;

            for (int i = 0; i < len; i++)
            {
                Data[0] = datatosend[i * 8 + 0];
                Data[1] = datatosend[i * 8 + 1];
                Data[2] = datatosend[i * 8 + 2];
                Data[3] = datatosend[i * 8 + 3];
                Data[4] = datatosend[i * 8 + 4];
                Data[5] = datatosend[i * 8 + 5];
                Data[6] = datatosend[i * 8 + 6];
                Data[7] = datatosend[i * 8 + 7];
                int temp = packet_yu - i * 8;
                if (temp < 8)
                    Send(0x7F, temp, Data);
                else
                    Send(0x7F, 8, Data);
            }
            //计算CRC
            Data = CRC.CRC32_byte(datatosend, packet_yu);
            //发送校验
            Send(0x7F, Data.Length, Data);
        }

        unsafe public void Send(int id, int len, byte[] Data)
        {
            if (m_bOpen == 0)
                return;

            VCI_CAN_OBJ sendobj = new VCI_CAN_OBJ();
            //sendobj.Init();
            sendobj.SendType = (byte)comboBox_SendType.SelectedIndex;
            sendobj.RemoteFlag = (byte)comboBox_FrameFormat.SelectedIndex;
            sendobj.ExternFlag = (byte)comboBox_FrameType.SelectedIndex;
            sendobj.ID = System.Convert.ToByte(id);

            sendobj.DataLen = System.Convert.ToByte(len);
            for (int i = 0; i < len; i++)
            {
                sendobj.Data[i] = Data[i];
            }

            if (VCI_Transmit(m_devtype, m_devind, m_canind, ref sendobj, 1) == 0)
            {
                richbox.Msg(Color.Red, "{0}发送失败！", DateTime.Now.ToString("HH:mm:ss fff"));
            }
        }

        public void ScanDevice()
        {
            byte[] Data = new byte[8];
            Data[0] = 0x01;
            Data[1] = 0;
            Data[2] = 0;
            Data[3] = 0;
            Data[4] = 0;
            Data[5] = 0;
            Data[6] = 0;
            Data[7] = 0;

            Send(0x7D, 8, Data);
        }
        unsafe void buttonScanDevice_Click(object sender, EventArgs e)
        {
            ScanDevice();
        }

        private void label17_Click(object sender, EventArgs e)
        {

        }

        private void txttargetIC_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void 说明ToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FormHelpExplain formHelpExplain = new FormHelpExplain();
            formHelpExplain.ShowDialog();
        }

        private void buttonClrDevicelist_Click(object sender, EventArgs e)
        {
            exListView1.Items.Clear();
            exListView1.LastIndex = -1;
        }

        private void buttonClrMsg_Click(object sender, EventArgs e)
        {
            textBoxMsg.Text = string.Empty;
        }

        private void buttonClrListboxInfo_Click(object sender, EventArgs e)
        {
            listBox_Info.Items.Clear();
        }
        /// <summary>
        /// 双击设备列表
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exListView1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            var items = this.exListView1.SelectedItems;
            if (items.Count == 1)
            {
                switch (items[0].SubItems[3].Text)
                {
                    case "磁导航-8":
                    case "磁导航-16":
                        FormCDH form = new FormCDH(items[0].SubItems[1].Text, items[0].SubItems[2].Text);//传递芯片ID,CANID
                        form.ShowDialog();
                        break;
                    case "地标":
                        FormRFID formRFID = new FormRFID(items[0].SubItems[1].Text, items[0].SubItems[2].Text);//传递芯片ID,CANID
                        formRFID.ShowDialog();
                        break;
                    case "遥控器":
                        FormControl formControl = new FormControl(items[0].SubItems[1].Text, items[0].SubItems[2].Text);//传递芯片ID,CANID
                        formControl.ShowDialog();
                        break;
                    case "超声波":
                        FormCSB formCSB = new FormCSB(items[0].SubItems[1].Text, items[0].SubItems[2].Text);//传递芯片ID,CANID
                        formCSB.ShowDialog();
                        break;
                    default: break;
                }
            }
        }

        private void exListView1_MouseClick(object sender, MouseEventArgs e)
        {
            txttargetIC.Text = exListView1.SelectedItems[0].SubItems[1].Text;
        }

        private void exListView1_DoubleClick(object sender, EventArgs e)
        {

        }
    }
}