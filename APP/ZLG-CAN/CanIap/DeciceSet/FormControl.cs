﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CanIAP.DeciceSet
{
    public partial class FormControl : Form
    {
        string DeviceID;
        int DeviecTimeOut = 2000;//读写超时时间 单位ms
        public FormControl(string deviceID, string canid)
        {
            InitializeComponent();
            DeviceID = deviceID;
            textBoxCanID.Text = canid;

            //读取所有配置
            Thread thread = new Thread(new ThreadStart(FunReadAll));
            thread.IsBackground = true;
            thread.Start();
        }
        //读取所有配置
        private void FunReadAll()
        {
            //ID
            FunReadId();
        }
        private void buttonSetID_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunSetId));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunSetId()
        {
            //判断ID是否为1-100的数值
            string patterns = @"^+?(([1-9]\d?)|(100)|(1))$";
            Regex regex = new Regex(patterns);
            if (regex.Match(textBoxCanID.Text).Success)
            {
                //修改ID
                byte[] Data = new byte[8];
                Data[0] = 0x01;
                Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
                Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
                Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
                Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);
                Data[5] = byte.Parse(regex.Match(textBoxCanID.Text).Value);
                Data[6] = 0;
                Data[7] = 0;

                //清除设置响应ID
                Form1.form1.SetResposeObj.ID = 0;
                Form1.form1.Send(0x78, 8, Data);
                //打印消息
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Green, ">修改ID为：{0}", Data[5]);
                Form1.form1.richbox.Msg(Color.Green, ">发送:[0X78] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

                Library.Time time = new Library.Time();
                Int64 nowtime = time.NowTimeMS();
                while ((Form1.form1.SetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
                {
                    Thread.Sleep(10);
                }
                if (time.NowTimeMS() - nowtime > DeviecTimeOut)
                {
                    MessageBox.Show("设备响应超时！");
                }
                else
                if (Form1.form1.SetResposeObj.Data[0] == 0x81)
                {
                    Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                    if (Form1.form1.SetResposeObj.Data[5] == 1)
                    {
                        Form1.form1.richbox.Msg(Color.Green, ">{0}", "设置成功!");
                        Form1.form1.ScanDevice();
                    }
                    else
                        Form1.form1.richbox.Msg(Color.Red, ">{0}", "设置失败！");
                }
                else
                {
                    Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                    Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
                }
            }
            else
                MessageBox.Show("请输入1-100的数值！");
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Thread thread = new Thread(new ThreadStart(FunReadId));
            thread.IsBackground = true;
            thread.Start();
        }
        unsafe private void FunReadId()
        {
            byte[] Data = new byte[8];
            Data[0] = 0x01;
            Data[1] = Convert.ToByte(DeviceID.Substring(0, 2), 16);
            Data[2] = Convert.ToByte(DeviceID.Substring(2, 2), 16);
            Data[3] = Convert.ToByte(DeviceID.Substring(4, 2), 16);
            Data[4] = Convert.ToByte(DeviceID.Substring(6, 2), 16);

            //清除设置响应ID
            Form1.form1.GetResposeObj.ID = 0;
            Form1.form1.Send(0x7A, 8, Data);
            //打印消息
            Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
            Form1.form1.richbox.Msg(Color.Green, ">读取ID！");
            Form1.form1.richbox.Msg(Color.Green, ">发送:[0X7A] {0}", BitConverter.ToString(Data, 0).Replace("-", " ").ToUpper());

            Library.Time time = new Library.Time();
            Int64 nowtime = time.NowTimeMS();
            while ((Form1.form1.GetResposeObj.ID == 0) && (time.NowTimeMS() - nowtime <= DeviecTimeOut))
            {
                Thread.Sleep(10);
            }
            if (time.NowTimeMS() - nowtime > DeviecTimeOut)
            {
                MessageBox.Show("设备响应超时！");
            }
            else
            if (Form1.form1.GetResposeObj.Data[0] == 0x91)
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Fuchsia, ">ID:{0}", Form1.form1.GetResposeObj.Data[5]);
                //更新到界面
                textBoxCanID.Text = Form1.form1.GetResposeObj.Data[5].ToString();
            }
            else
            {
                Form1.form1.richbox.Msg(Color.Blue, ">{0}", DateTime.Now.ToString("HH:mm:ss fff"));
                Form1.form1.richbox.Msg(Color.Red, ">{0}", "未知错误，总线数据冲突！");
            }
        }


        private void buttonCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
