﻿namespace CanIAP.DeciceSet
{
    partial class FormControl
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.buttonCancel = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label9 = new System.Windows.Forms.Label();
            this.button10 = new System.Windows.Forms.Button();
            this.buttonSetID = new System.Windows.Forms.Button();
            this.textBoxCanID = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // buttonCancel
            // 
            this.buttonCancel.Font = new System.Drawing.Font("宋体", 12F);
            this.buttonCancel.Location = new System.Drawing.Point(297, 236);
            this.buttonCancel.Name = "buttonCancel";
            this.buttonCancel.Size = new System.Drawing.Size(86, 29);
            this.buttonCancel.TabIndex = 38;
            this.buttonCancel.Text = "确定";
            this.buttonCancel.UseVisualStyleBackColor = true;
            this.buttonCancel.Click += new System.EventHandler(this.buttonCancel_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.button10);
            this.groupBox1.Controls.Add(this.buttonSetID);
            this.groupBox1.Controls.Add(this.textBoxCanID);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("宋体", 12F);
            this.groupBox1.Location = new System.Drawing.Point(11, 9);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(382, 221);
            this.groupBox1.TabIndex = 37;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "参数设置";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.ForeColor = System.Drawing.SystemColors.ControlDarkDark;
            this.label9.Location = new System.Drawing.Point(181, 31);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 16);
            this.label9.TabIndex = 51;
            this.label9.Text = "(1-100)";
            // 
            // button10
            // 
            this.button10.Font = new System.Drawing.Font("宋体", 12F);
            this.button10.Location = new System.Drawing.Point(321, 25);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(51, 28);
            this.button10.TabIndex = 43;
            this.button10.Text = "读取";
            this.button10.UseVisualStyleBackColor = true;
            this.button10.Click += new System.EventHandler(this.button10_Click);
            // 
            // buttonSetID
            // 
            this.buttonSetID.Font = new System.Drawing.Font("宋体", 12F);
            this.buttonSetID.Location = new System.Drawing.Point(264, 25);
            this.buttonSetID.Name = "buttonSetID";
            this.buttonSetID.Size = new System.Drawing.Size(51, 28);
            this.buttonSetID.TabIndex = 26;
            this.buttonSetID.Text = "设置";
            this.buttonSetID.UseVisualStyleBackColor = true;
            this.buttonSetID.Click += new System.EventHandler(this.buttonSetID_Click);
            // 
            // textBoxCanID
            // 
            this.textBoxCanID.Location = new System.Drawing.Point(92, 26);
            this.textBoxCanID.Name = "textBoxCanID";
            this.textBoxCanID.Size = new System.Drawing.Size(89, 26);
            this.textBoxCanID.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(22, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "CAN-ID:";
            // 
            // FormControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(404, 275);
            this.Controls.Add(this.buttonCancel);
            this.Controls.Add(this.groupBox1);
            this.Name = "FormControl";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "遥控器";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button buttonCancel;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.Button buttonSetID;
        private System.Windows.Forms.TextBox textBoxCanID;
        private System.Windows.Forms.Label label1;
    }
}